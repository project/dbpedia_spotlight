<?php

  $id = 0;
  $nodes = array();
  $edges = array();

  foreach ($terms as $term) {
    $id++;
    $node = new stdClass();
    $node->id = $id;
    $node->label = $dbpedia_uri . $term->name;
    $node->font = '8px courier';
    array_push($nodes, $node);
  }

  $j = $id + 1;
  $node = new stdClass();
  $node->id = $j;
  $node->label = $uri;
  $node->shape = 'box';
  $node->color = 'rgb(255,168,7)';
  array_push($nodes, $node);

  $id = 0;
  foreach ($terms as $term) {
    $id++;
    $edge = new stdClass();
    $edge->from = $j;
    $edge->to = $id;
    $edge->label = $rdf_property;
    //$edge->title = $rdf_property;
    $edge->font = '8px courier';
    array_push($edges, $edge);
  }


?>


<div id="mynetwork" style="height: 600px;"></div>

<script type="text/javascript">
  // create an array with nodes
  var nodes = <?php print drupal_json_encode($nodes) ?>;

  // create an array with edges
  var edges = <?php print drupal_json_encode($edges) ?>;

  // create a network
  var container = document.getElementById('mynetwork');
  var data = {
    nodes: nodes,
    edges: edges
  };

  var options = {
    nodes : {
      shape: 'dot',
      size: 10
    }
  };

  var network = new vis.Network(container, data, options);

</script>

